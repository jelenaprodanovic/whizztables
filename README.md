# WhizzTables

The project is developed as a test project.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

What things you need to install the software and how to install them

```
nodeJS
npm
```

### Clone the repository

To clone the repository from bitbucket, enter the following command:

```
git clone git@bitbucket.org:jelenaprodanovic/whizztables.git
```

### Before starting the project

```
npm run install
```

### Build and start the project

There are two separate builds, one for development and one for production purposes.

To build the project in development, run:

```
npm run build-dev
```

To build the project in production, run:

```
npm run build-prod
```

### Start webpack-dev-server

To start local development server, run:

```
npm run start
```

In development mode, development server is started on port 3000.
There is another test file added as source for data, it can be seen on [http://localhost:3000/#!/table/test](http://localhost:3000/#!/table/test).


## Remarks

Code style is inspired by [style guide](https://github.com/toddmotto/angularjs-styleguide) developed by Angular Expert Todd Motto, for developing modern ES6 + Angular 1.6 applications.
Project is based on AngularJS v1.6.9, Typescript v2.8.1, Webpack v4.4.1, and Babel for transpiling.
Reasons for using this code style are [numerous](https://toddmotto.com/rewriting-angular-styleguide-angular-2), but the main are:

1. Using modules, import and export (no IIFE, global scope pollution, etc)
2. Using component and class architecture
3. No $scope, no controllerAs
4. Routed components
5. (relatively) Easy transition to Angular



