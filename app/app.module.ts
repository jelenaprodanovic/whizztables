import * as angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import datePicker from 'angular-ui-bootstrap/src/datepicker';
import datePickerPopup from 'angular-ui-bootstrap/src/datepickerPopup';
import { AppComponent }  from './app.component';

// Styles
import './app.scss';

// Components
import { ComponentsModule } from './components/components.module';

// Common
import { CommonModule } from './common/common.module';

// Services
import { ServicesModule } from './services/services.module';

// Filters
import { FiltersModule } from './filters/filters.module';

export const AppModule = angular
  .module('app', [
    ComponentsModule,
    CommonModule,
    FiltersModule,
    ServicesModule,
    uiRouter,
    datePicker,
    datePickerPopup
  ])
  .component('app', AppComponent)
  .name;