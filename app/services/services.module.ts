import * as angular from 'angular';
import { CommonService } from './common.service';
import { DataService } from './data.service';

export const ServicesModule = angular
  .module('app.services', [])
  .service('CommonService', CommonService)
  .service('DataService', DataService)
  .name;