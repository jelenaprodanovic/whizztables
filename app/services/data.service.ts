import * as rx from 'rx';
const DATA_PATH: string = "./data/";
export class DataService {
    $http: angular.IHttpService;
    $state: any;
    $log: angular.ILogService;

    commonService: any;
    recentTableRoutes: any[] = [];
    subject: any; 

    constructor($http: angular.IHttpService, $state: any, $log: angular.ILogService, CommonService: any) {
        'ngInject';
        this.$http = $http;
        this.$state = $state;
        this.$log = $log;
        this.commonService = CommonService;

        this.subject = new rx.Subject();
    }

    /////////////////

    getData = (fileName: string) => {
        return this.$http.get(`${DATA_PATH}${fileName}.json`)
            .then(response => response.data,
            error => {
                this.$log.error(error);
                this.$state.go('404');
            });
    }

    getRecentTableRoutes = () => {
        return this.recentTableRoutes;
    }

    setRecentTableRoutes = (path: string) => {        
        if (!this.recentTableRoutes.some((x) => x.sref === path)) {
            let obj = { 'name': this.commonService.prettifyString(path), 'sref': path, 'active': true };
            this.recentTableRoutes.map((el) => el.active = false);
            this.recentTableRoutes.push(obj);
            this.subject.onNext(this.recentTableRoutes);
        };
    }

    subscribe = (x: any) => {
        return this.subject.subscribe(x);
    }
}