export class CommonService {

    constructor() { }

    /////////////////

    prettifyString = (string: string) => {
        if (string.length) {
            string = string.replace(/_/g, ' ');
            return string.trim().charAt(0).toUpperCase() + string.slice(1);
        } else {
            return string;
        }
    }
}