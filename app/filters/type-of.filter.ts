import { CommonService } from './../services/common.service';
import * as angular from 'angular';
import moment from 'moment';

export default function (CommonService: any) {
    return function (obj: any) {
        let type: string;
        if (obj === undefined || obj === null) {
            return 'undefined';
        }

        if (typeof obj === 'number') {
            if (isFinite(obj) && Math.floor(obj) === obj) {
                return 'integer';
            }
            if (!!(obj % 1)) {
                return 'decimal';
            }
        } else if (typeof obj === 'string' && obj.length) {
            let regexp = /^\$[0-9\.]+/;
            if (regexp.test(obj)) {
                return 'currency';
            }

            regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            if (regexp.test(obj)) {
                return 'url';
            }

            regexp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (regexp.test(obj)) {
                return 'email';
            }

            let formats = [
                moment.ISO_8601,
                'MM/DD/YYYY',
                'DD/MM/YYYY',
                'm/d/YYYY',
                'd/m/YYYY'
            ];

            if (moment(obj, formats, true).isValid()) {
                return 'date';
            }

            if (angular.isObject(obj)) {
                return 'object';
            }

            return 'string';
        } else {
            return typeof obj;
        }
    }
}