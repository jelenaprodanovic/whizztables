import moment from 'moment';

export default function () {
    return function (items: any, search: any) {
        if (!search || Object.keys(search).every((el) => search[el] === '')) {
            return items;
        }  
        
        for (let obj in search) {
            items = items.filter((item: any) => moment(item[obj]).isBetween(moment(search[obj]), moment(), 'day', '[]'));
        }

        return items;
    };
}