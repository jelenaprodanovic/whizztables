import * as angular from 'angular';
import typeOfFilter from './type-of.filter';
import dateFilter from './date.filter';

export const FiltersModule = angular
  .module('app.filters', [])
  .filter('typeOf', typeOfFilter)
  .filter('dateFilter', dateFilter)
  .name;