import template from './app.html';

export const AppComponent = {
  bindings: { },
  template: template,
  controller: class AppComponent {
    constructor() { }
  }
};