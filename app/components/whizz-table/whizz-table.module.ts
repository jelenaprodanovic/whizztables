import * as angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import { WhizzTableComponent } from './whizz-table.component';
import { DataService } from '../../services/data.service';
import { CommonService } from './../../services/common.service';
import './whizz-table.scss';

export const WhizzTableModule = angular
  .module('whizzTable', [ uiRouter ])
  .component('whizzTable', WhizzTableComponent)
  .service('DataService', DataService)
  .service('CommonService', CommonService)
  .config(($urlRouterProvider: any, $stateProvider: any) => {
    'ngInject';
    
    $stateProvider
        .state('table', {
            url: '/table/:fileName',
            component: 'whizzTable',            
            resolve: {
              tableData: (DataService: any, $stateParams: any) => DataService.getData($stateParams.fileName),
              pageTitle: ($stateParams: any, CommonService: any) => CommonService.prettifyString($stateParams.fileName)
            }
        });
      }) 
  .name;