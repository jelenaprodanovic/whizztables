import template from './whizz-table.html';

class WhizzTableController {
  commonService: any;
  dataService: any;
  filter: any;
  isCalendarOpened: boolean = false;
  pageTitle: string;
  searchDate: any = {};
  searchObject: string;
  searchFilter: any = {};
  sortReverse: boolean = false;
  sortType: string;
  tableData: any[];
  tableHeaderFields: any[];

  constructor(CommonService: any, typeOfFilter: any, $stateParams: any, DataService: any) {
    'ngInject';
    this.commonService = CommonService;
    this.dataService = DataService;
    this.filter = typeOfFilter;
    this.dataService.setRecentTableRoutes($stateParams.fileName);
  }

  $onInit = () => {
    this.tableHeaderFields = this.getHeaderFields(this.tableData);
    this.tableData.map((el) => el.types = new Object());
    this.tableData.map((el) => Object.keys(el).filter((el) => el !== 'types').map((key) => el['types'][key] = this.filter(el[key])));
    
    this.sortType = this.tableHeaderFields?this.tableHeaderFields[0].value : '';
    this.tableHeaderFields.map((el) => el.type = this.tableData[0].types[el.value]);
    this.tableHeaderFields.map((x) => x.isCalendarOpened = false);
  }

  /////////////////

  getHeaderFields = (data: any) => {
    if (data.length && data[0]) {
      return Object.getOwnPropertyNames(data[0]).map((k) => ({'value': k, 'pretty': this.commonService.prettifyString(k)}));
    }
  }

  resetSearch = (fieldName: string, filterObject: any) => {
    if (!fieldName || !filterObject[fieldName]) {
      delete filterObject[fieldName];
    }
  }
}

export const WhizzTableComponent = {
  bindings: {
    tableData: '<',
    pageTitle: '@'
  },
  template: template,
  controller: WhizzTableController
};