import * as angular from 'angular';
import { Page404Module } from './page-404/page-404.module';
import { HomePageModule } from './home-page/home-page.module';

export const StaticPagesModule = angular
  .module('app.static', [
      Page404Module,
      HomePageModule
  ])
  .name;