import * as angular from 'angular';
import { Page404Component } from './page-404.component';
import './page-404.scss';

export const Page404Module = angular
  .module('page404', [])
  .component('page404', Page404Component)
  .config(($urlRouterProvider: any, $stateProvider: any) => {
    'ngInject';

    $stateProvider
      .state('404', {
        url: '/404',
        component: 'page404'
      });
  })
  .name;