import template from './page-404.html';

export const Page404Component = {
  bindings: { },
  template: template,
  controller: class Page404Component {
    constructor() { }
  }
};