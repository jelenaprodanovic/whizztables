import * as angular from 'angular';
import { HomePageComponent } from './home-page.component';
import './home-page.scss';

export const HomePageModule = angular
  .module('homePage', [])
  .component('homePage', HomePageComponent)
  .config(($urlRouterProvider: any, $stateProvider: any) => {
    'ngInject';

    $stateProvider
      .state('home', {
        url: '/',
        component: 'homePage'
      });
      
      $urlRouterProvider.otherwise('/404');
  })
  .name;