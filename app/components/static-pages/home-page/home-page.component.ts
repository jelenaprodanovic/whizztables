import template from './home-page.html';

export const HomePageComponent = {
  bindings: { },
  template: template,
  controller: class HomePageComponent {
    constructor() { }
  }
};