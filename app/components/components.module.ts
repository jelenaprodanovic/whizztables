import * as angular from 'angular';
import { WhizzTableModule } from './whizz-table/whizz-table.module';
// Static pages
import { StaticPagesModule } from './static-pages/static-pages.module';

export const ComponentsModule = angular
  .module('app.components', [
    WhizzTableModule,
    StaticPagesModule
  ])
  .name;