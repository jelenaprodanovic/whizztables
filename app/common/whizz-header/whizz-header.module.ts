import * as angular from 'angular';
import { WhizzHeaderComponent } from './whizz-header.component';
import './whizz-header.scss';

export const WhizzHeaderModule = angular
  .module('whizzHeader', [])
  .component('whizzHeader', WhizzHeaderComponent)
  .name;