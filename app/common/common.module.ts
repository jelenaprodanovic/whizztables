import * as angular from 'angular';
import { WhizzHeaderModule } from './whizz-header/whizz-header.module';
import { WhizzSidebarModule } from './whizz-sidebar/whizz-sidebar.module';
// import { WhizzFooterModule } from './whizz-footer/whizz-footer.module';

export const CommonModule = angular
  .module('app.common', [
    WhizzHeaderModule,
    WhizzSidebarModule,
    // WhizzFooterModule
  ])
  .name;