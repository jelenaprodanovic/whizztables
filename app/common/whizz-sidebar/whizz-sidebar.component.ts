import template from './whizz-sidebar.html';

class WhizzSidebarController {
  dataService: any;
  fileList: any[];
  subscriber: any;

  constructor(DataService: any) { 
    this.dataService = DataService;
  }

  /////////////////

  $onInit = () => {
    this.subscriber = this.dataService.subscribe((obj: any) => {
      this.fileList = obj;
    });
  }

  setActive = (file: any) => {
    this.fileList.map((el) => el.active = false);
    this.fileList.find((obj) => obj.name === file.name).active = true;
  }
}

export const WhizzSidebarComponent = {
  bindings: {},
  template: template,
  controller: WhizzSidebarController
}