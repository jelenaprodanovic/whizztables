import * as angular from 'angular';
import { WhizzSidebarComponent } from './whizz-sidebar.component';
import './whizz-sidebar.scss';

export const WhizzSidebarModule = angular
  .module('whizzSidebar', [])
  .component('whizzSidebar', WhizzSidebarComponent)
  .name;